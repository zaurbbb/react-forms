import React from "react";

import User from "./components/User";

import "./App.css";

const App = () => {
  return (
    <div className="app">
      <User />
    </div>
  );
};

export default App;
