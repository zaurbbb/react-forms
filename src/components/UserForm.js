import React from "react";
import {
  ErrorMessage,
  Field,
  Form
} from "formik";
import UserData from "./UserData";

const UserForm = ({ formik }) => {
  const {
          initialValues,
          dirty,
          handleReset,
          values,
          handleSubmit,
        } = formik;

  return (
    <Form
      className="form"
      onSubmit={handleSubmit}
    >
      <div className="row">
        <label htmlFor="firstName">First Name</label>
        <Field
          id="firstName"
          name="firstName"
          placeholder="First Name"
          value={values.firstName}
        />
      </div>

      <div className="row">
        <label htmlFor="lastName">Last Name</label>
        <Field
          id="lastName"
          name="lastName"
          placeholder="Last Name"
        />
      </div>

      <div className="row">
        <label htmlFor="age">Age</label>
        <Field
          id="age"
          name="age"
          placeholder="Age"
        />
      </div>

      <div className="row">
        <label htmlFor="employed">Employed</label>
        <Field
          type="checkbox"
          name="employed"
          value="True"
        />
      </div>

      <div className="row">
        <label htmlFor="favoriteColor">Favorite color</label>
        <Field
          as="select"
          name="favoriteColor"
          value={initialValues.favoriteColor}
        >
          <option value="red">Red</option>
          <option value="green">Green</option>
          <option value="blue">Blue</option>
        </Field>

      </div>

      <div className="row">
        <label htmlFor="sauces">Sauces</label>
        <div className="list">
          <label>
            <Field
              type="checkbox"
              name="sauces"
              value="Ketchup"
            />
            <span>Ketchup</span>
          </label>
          <label>
            <Field
              type="checkbox"
              name="sauces"
              value="Mustard"
            />
            <span>Mustard</span>
          </label>
          <label>
            <Field
              type="checkbox"
              name="sauces"
              value="Mayonnaise"
            />
            <span>Mayonnaise</span>
          </label>
          <label>
            <Field
              type="checkbox"
              name="sauces"
              value="Guacamole"
            />
            <span>Guacamole</span>
          </label>
        </div>
      </div>

      <div className="row">
        <label htmlFor="stooge">Best Stooge</label>
        <div className="list">
          <label>
            <Field
              type="radio"
              name="stooge"
              value="Larry"
            />
            <span>Larry</span>
          </label>
          <label>
            <Field
              type="radio"
              name="stooge"
              value="Moe"
            />
            <span>Moe</span>
          </label>
          <label>
            <Field
              type="radio"
              name="stooge"
              value="Curly"
            />
            <span>Curly</span>
          </label>
        </div>
      </div>

      <div className="row">
        <label htmlFor="notes">Notes</label>
        <Field
          as="textarea"
          name="notes"
        />
      </div>

      <div className="error-block">
        <span><ErrorMessage name="firstName" /></span>
        <span><ErrorMessage name="lastName" /></span>
        <span><ErrorMessage name="age" /></span>
        <span><ErrorMessage name="favoriteColor" /></span>
        <span><ErrorMessage name="sauces" /></span>
        <span><ErrorMessage name="stooge" /></span>
        <span><ErrorMessage name="notes" /></span>
      </div>

      <div style={{ textAlign: "center" }}>
        <input
          type="submit"
          disabled={!dirty}
          className="button button--submit"
          value="Submit"
        />
        <button
          type="button"
          onClick={handleReset}
          disabled={!dirty}
          className="button button--reset"
        >
          Reset
        </button>
      </div>
      <UserData values={values} />
    </Form>
  );
}

export default UserForm;
