import React from "react";

const UserData = ({values}) => {
  return (
    <pre className="user-data">
      {JSON.stringify(values, null, 2)}
    </pre>
  );
};

export default UserData;
