import React from 'react';

import { Formik } from 'formik';
import UserForm from "./UserForm";

const User = () => {
  const initialValues = {
    firstName: '',
    lastName: '',
    age: 0,
    favoriteColor: 'red',
    sauces: [],
    stooge: 'Larry',
    notes: '',
  }

  function onSubmit(values) {
    alert(JSON.stringify(values, null, 2));
  }

  function validate(values) {
    const errors = {};

    if (!values.firstName) {
      errors.firstName = 'First Name is required';
    } else if (!/^[a-zA-Z ]*$/.test(values.firstName)) {
      errors.firstName = 'First Name: only alphabet characters and spaces allowed';
    }

    if (!values.lastName) {
      errors.lastName = 'Last Name is required';
    } else if (!/^[a-zA-Z ]*$/.test(values.lastName)) {
      errors.lastName = 'Last Name: only alphabet characters and spaces allowed';
    }

    if (!values.age) {
      errors.age = 'Age is required';
    } else if (!/^\d+$/.test(values.age)) {
      errors.age = "Age should contain only numbers";
    }

    if (!values.favoriteColor) {
      errors.favoriteColor = 'Favorite Color is required';
    }

    if (values.sauces.length === 0) {
      errors.sauces = 'Sauces is required';
    }

    if (!values.stooge) {
      errors.stooge = 'Stooge is required';
    }

    if (values.notes.length < 100) {
      errors.notes = 'Notes must not exceed 100 characters';
    }

    return errors;
  }


  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validate={validate}
      >
        {(formik) => (
          <UserForm formik={formik} />
        )}
      </Formik>
    </>
  );
};

export default User;
